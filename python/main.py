import os


def merge_md_files(directory):
    # Get all the .md files in the directory
    md_files = [f for f in os.listdir(directory) if f.endswith('.md')]

    # Sort the files alphabetically
    md_files.sort()

    # Merge the files
    merged_content = ""
    for file in md_files:
        with open(os.path.join(directory, file), 'r', encoding='utf-8') as f:
            merged_content += f.read() + "\n\n"

    # Write the merged content to a new file
    with open('merged.md', 'w', encoding='utf-8') as f:
        f.write(merged_content)


# Call the function
directory_path = 'Narrative/Sessions/Session L (7.22)'  # or specify the path to your directory
merge_md_files(directory_path)
